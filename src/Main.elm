module Main exposing (Msg(..), main, update, view)

import Browser
import Browser.Dom exposing (Element, getElement)
import Browser.Events exposing (onAnimationFrame, onMouseMove)
import Html exposing (Html, button, div, img, li, text, ul)
import Html.Attributes exposing (attribute, id, src, style)
import Json.Decode exposing (Decoder, field, int, map, map2)
import Task
import Time


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Point =
    { x : Int
    , y : Int
    }


type alias Model =
    { mouse : Point
    , center : Point
    , delta : Point
    , ratio : Int
    , containerId : String
    }


init : () -> ( Model, Cmd Msg )
init _ =
    let
        containerId =
            "parallax"
    in
    ( Model (Point 0 0) (Point 0 0) (Point 0 0) 10 containerId, Task.attempt NewContainerCenter <| getElement containerId )


type Msg
    = Tick Time.Posix
    | MouseMove Point
    | NewContainerCenter (Result Browser.Dom.Error Element)


update msg model =
    case msg of
        MouseMove newMouse ->
            ( { model | mouse = newMouse }
            , Cmd.none
            )

        Tick _ ->
            ( { model | delta = substract model.mouse model.center }
            , Cmd.none
            )

        NewContainerCenter result ->
            case result of
                Ok container ->
                    ( { model | center = getContainerCenter container.element }
                    , Cmd.none
                    )

                Err _ ->
                    ( model, Cmd.none )


subscriptions model =
    Sub.batch
        [ onMouseMove <| map MouseMove mouseDecoder
        , onAnimationFrame Tick
        ]


mouseDecoder : Decoder Point
mouseDecoder =
    map2 Point
        (field "clientX" int)
        (field "clientY" int)


createLayer model i =
    img
        [ src <| String.concat [ "/assets/images/layer", String.fromInt i, ".png" ]
        , style "position" "absolute"
        , style "width" "100%"
        , style "top" <| String.concat [ String.fromInt <| model.delta.y * i // model.ratio, "px" ]
        , style "left" <| String.concat [ String.fromInt <| model.delta.x * i // model.ratio, "px" ]
        ]
        []


layers model =
    List.range 1 6 |> List.map (createLayer model)


getContainerCenter container =
    Point
        (round <| container.x + container.width / 2)
        (round <| container.y + container.height / 2)


substract minuend subtrahend =
    Point
        (minuend.x - subtrahend.x)
        (minuend.y - subtrahend.y)


viewParallax model =
    div
        [ id model.containerId
        , style "position" "relative"
        , style "width" "256px"
        , style "height" "256px"
        , style "margin" "0 auto"
        ]
        [ div [] <| layers model
        ]


view model =
    div
        []
        [ ul []
            [ li [] [ text <| String.concat [ "mouse: x = ", String.fromInt model.mouse.x, " y = ", String.fromInt model.mouse.y ] ]
            , li [] [ text <| String.concat [ "center: x = ", String.fromInt model.center.x, " y = ", String.fromInt model.center.y ] ]
            , li [] [ text <| String.concat [ "delta: x = ", String.fromInt model.delta.x, " y = ", String.fromInt model.delta.y ] ]
            ]
        , div [ style "margin-top" "128px" ]
            [ viewParallax model
            ]
        ]
